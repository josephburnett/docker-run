package main

import (
	"flag"
	"os"
	"os/exec"
	"strings"
)

var (
	image    = flag.String("image", "", "")
	command  = flag.String("command", "", "")
	stepPath = flag.String("step_path", "", "")
)

func main() {
	flag.Parse()
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	args := []string{
		"run",
	}
	if *stepPath != "-" {
		args = append(args, []string{
			"-w", *stepPath,
			"-v", *stepPath + ":" + *stepPath,
		}...)
	}
	args = append(args, []string{
		"-v", cwd + ":" + cwd,
		*image,
	}...)
	commandFields := strings.Split(*command, "\n")
	args = append(args, commandFields...)
	cmd := exec.Command("docker", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		panic(err)
	}
}
